import { StepFunctions } from 'aws-sdk';
import { lambda } from 'lambda-api-helper';

const stepFunctions = new StepFunctions();

export const schedule = lambda.extend(async (request, response) => {
  const { prefix, youtubeId } = request.body;

  const stateMachineArn = process.env.STEP_FUNCTION_ARN;

  const result = await stepFunctions
    .startExecution({
      stateMachineArn,
      input: JSON.stringify({
        prefix: (prefix as string).endsWith('/') ? prefix : `${prefix}/`,
        youtubeId,
      }),
    })
    .promise();

  console.log(`State machine ${stateMachineArn} invoked successfully`, result);

  response.status(201);
  return { result };
});

export const status = lambda.extend(async (request, response) => {
  const { requestId } = request.query;

  const stateMachineArn = process.env.STEP_FUNCTION_ARN;
  const executionArnPrefix = stateMachineArn.replace(
    'stateMachine',
    'execution'
  );

  const result = await stepFunctions
    .describeExecution({
      executionArn: `${executionArnPrefix}:${requestId}`,
    })
    .promise();

  return {
    result: {
      status: result.status,
    },
  };
});
