import { S3 } from 'aws-sdk';
import { spawn } from 'child_process';
import * as fs from 'fs';
import { nanoid } from 'nanoid';
import { PassThrough, Readable } from 'stream';
import * as ytdl from 'ytdl-core';

const ffmpeg = process.env.FFMPEG_PATH ?? 'ffmpeg';

async function merge(
  filename: string,
  audio: Readable,
  video: Readable
): Promise<{ path: string }> {
  return new Promise((resolve) => {
    const path = `/tmp/${filename}.mp4`;

    // Start the ffmpeg child process
    const ffmpegProcess = spawn(
      ffmpeg,
      [
        // Remove ffmpeg's console spamming
        '-loglevel',
        '8',
        '-hide_banner',
        // Set inputs
        '-i',
        'pipe:3',
        '-i',
        'pipe:4',
        // Map audio & video from streams
        '-map',
        '0:a',
        '-map',
        '1:v',
        // Keep encoding
        '-c:v',
        'copy',
        // Define output file
        path,
      ],
      {
        windowsHide: true,
        stdio: [
          /* Standard: stdin, stdout, stderr */
          'inherit',
          'inherit',
          'inherit',
          /* Custom: pipe:3, pipe:4 */
          'pipe',
          'pipe',
        ],
      }
    );

    ffmpegProcess.on('close', () => {
      resolve({ path });
    });

    // @ts-ignore
    audio.pipe(ffmpegProcess.stdio[3]);
    // @ts-ignore
    video.pipe(ffmpegProcess.stdio[4]);
  });
}

async function passThroughS3(
  download,
  bucket: string,
  key: string,
  uploadProgress: (progress) => void = () => {}
): Promise<void> {
  const passThrough = new PassThrough();

  const upload = new S3.ManagedUpload({
    params: {
      Bucket: bucket,
      Key: key,
      Body: passThrough,
    },
    partSize: 1024 * 1024 * 64, // in bytes
  });

  upload.on('httpUploadProgress', uploadProgress);

  download.pipe(passThrough);

  await upload.promise();
}

export async function handle({
  prefix,
  youtubeId,
}: {
  prefix: string;
  youtubeId: string;
}) {
  console.log('getting youtube info');

  const info = await ytdl.getInfo(
    `https://www.youtube.com/watch?v=${youtubeId}`
  );
  const title = info.videoDetails.title;

  console.log('got info, going to download video');

  const { path } = await merge(
    nanoid(),
    ytdl.downloadFromInfo(info, { quality: 'highestaudio' }),
    ytdl.downloadFromInfo(info, { quality: 'highestvideo' })
  );

  console.log('downloaded and merge file', { path });

  const bucket = process.env.S3_BUCKET ?? 'youtube-download-dev-upload';
  const sanitizedTitle = `${title.replace(new RegExp(/\//, 'g'), '_')}.mp4`;
  const key = `${prefix}${sanitizedTitle}`;

  await passThroughS3(fs.createReadStream(path), bucket, key, (progress) =>
    console.log(`[${youtubeId}] copying video ...`, progress)
  );

  const result = {
    url: `s3://${bucket}/${key}`,
    title: sanitizedTitle,
    prefix,
  };

  console.log(`${youtubeId} finished uploading`, result);

  return result;
}
