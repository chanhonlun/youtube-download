import { DynamoDB } from 'aws-sdk';

export async function handle({
  url,
  title,
  prefix,
}: {
  url: string;
  title: string;
  prefix: string;
}) {
  const table = `${process.env.APP_NAME}-videos`;

  await new DynamoDB()
    .putItem({
      TableName: table,
      Item: {
        title: { S: title },
        prefix: { S: prefix },
        url: { S: url },
        createdAt: { S: new Date().toISOString() },
      },
    })
    .promise();
}
